(ns weather-app.core
  (:require [clj-http.client :as client]
            [clojure.string :as string]
            [clojure.data.json :as json]))

(def weather-api-key (atom ""))
(if (empty? @weather-api-key)
    (do
        (println "Please provide an API key")
        (System/exit 0)))
(def weather-api-host (atom (str "http://api.wunderground.com/api/" @weather-api-key "/")))

; (defn call-api
;     "call the weather API directly to get the weather reports"
;     [url]
;     ;** one solusion: calling the api directly
;     ; (println (client/get (string/join [@weather-api-host config])))
; 
;     ;** second solusion: make the code a bit more understandable
;     ; (println
;     ;     (let [url (string/join [@weather-api-host config])]
;     ;         (client/get url)))
; )

;** third solusion (better): I have decided to break them down into different functions
;** so that we have a more dynamic function on making a http call
(defn get-http-call
    "GET HTTP call"
    [url]
    (println "GET" url)
    (if (empty? url) "The `url` is not specified" (client/get url))
)

(defn weather-api-call
    [request-options]
    (get-http-call (string/join [@weather-api-host request-options])))

(defn get-json-weather-report-type1
    "Get the current conditions of a place"
    [features settings location]
    (->>
        (let [jsonLocation (string/join [location ".json"])]
            (remove nil?
                (concat features settings ["q" jsonLocation])))
        (string/join "/")
        (weather-api-call))
)

(defn get-json-weather-report-type2
    "Get the current conditions of a place"
    [& args]
    (->>
        (string/join "." [(string/join "/" (remove nil? args)) "json"])
        (weather-api-call)
    )
)

(defn get-json-value
    "Get the value of the requested key from a json object"
    [json-obj requested-key]
    (println requested-key)
    (get (json/read-str json-obj) requested-key)
)

;////////////////////////////////

; The language extractor
(defn get-lang 
    "Retiereve the language from the given string"
    [str]
    (if (not-empty str)
        (get (re-find #"^.*?lang:(.*?)(\/.*|$)" str) 1)
        "URL string is empty"))

(defn -main
    "Running test cases"
    [& args]
    (println (get-json-weather-report-type1 ["conditions"] nil "CA/San_Francisco"))
    ; (get-json-weather-report-type1 ["conditions" "forecast"] nil "CA/San_Francisco")
    ; (get-json-weather-report-type1 ["conditions"] ["lang:FR"] "CA/San_Francisco")
    ; (get-json-weather-report-type1 ["conditions"] ["lang:FR" "pwd:1"] "CA/San_Francisco")
    ; (get-json-weather-report-type1 ["conditions" "forecast"] ["lang:FR"] "CA/San_Francisco")

    (println (get-json-weather-report-type2 "conditions" "q/CA/San_Francisco"))
    ; (get-json-weather-report-type2 "conditions" "forecast" "q/CA/San_Francisco")
    ; (get-json-weather-report-type2 "conditions" "lang:FR" "q/CA/San_Francisco")
    ; (get-json-weather-report-type2 "conditions" "lang:FR" "pwd:1" "q/CA/San_Francisco")
    ; (get-json-weather-report-type2 "conditions" "forecast" "lang:FR" "q/CA/San_Francisco")

    (println (get-lang "conditions/lang:FR/?location=NY"))
    ; (get-lang "conditions/lang:FR")
    ; (get-lang "conditions/lang:FR/pwd:0/")
    ; (get-lang "")
    ; (get-lang "conditions/")
    )

;(-main)