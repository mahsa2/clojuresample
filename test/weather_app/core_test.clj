(ns weather-app.core-test
  (:require [clojure.test :refer :all]
            [weather-app.core :refer :all]))
(use 'clojure.data)

(deftest get-http-call-test
    (testing "get-http-call Test"
        (is (not (=
            (get-http-call "http://google.com")
            "The `url` is not specified")))))

(deftest get-http-call-error-test
    (testing "get-http-call empty url Test"
        (is (=
            (get-http-call "")
            "The `url` is not specified"))))

(deftest weather-api-call-not-empty-test
    (testing "weather-api-call Test (get result)"
        (is (not (empty?  (weather-api-call "conditions/q/CA/San_Francisco.json"))))))

(deftest get-json-weather-report-type1-not-empty-test
    (testing "get-json-weather-report-type1 Test (get result)"
        (is (not (empty? (get-json-weather-report-type1 ["conditions"] nil "CA/San_Francisco"))))))

(deftest get-json-weather-report-type2-not-empty-test
    (testing "get-json-weather-report-type2 Test (get result)"
        (is (not (empty? (get-json-weather-report-type2 "conditions" "q/CA/San_Francisco"))))))

(deftest get-json-weather-report-feature-no-setting-test
    (testing "Evaluate the result of the two reports when providing only one feature"
        (let [response1 (get-json-weather-report-type1 ["conditions"] nil "CA/San_Francisco")]
            (let [response2 (get-json-weather-report-type2 "conditions" "q/CA/San_Francisco")]
                    (is (.equals (:body response1) (:body response2)))))))

(deftest get-json-weather-report-features-no-setting-test
    (testing "Evaluate the result of the two reports when providing multiple features and no setting"
        (let [response1 (get-json-weather-report-type1 ["conditions" "forecast"] nil "CA/San_Francisco")]
            (let [response2 (get-json-weather-report-type2 "conditions" "forecast" "q/CA/San_Francisco")]
                    (is (.equals (:body response1) (:body response2)))))))

(deftest get-json-weather-report-feature-setting-test
    (testing "Evaluate the result of the two reports when providing one feature and one setting"
        (let [response1 (get-json-weather-report-type1 ["conditions"] ["lang:FR"] "CA/San_Francisco")]
            (let [response2 (get-json-weather-report-type2 "conditions" "lang:FR" "q/CA/San_Francisco")]
                    (is (.equals (:body response1) (:body response2)))))))

(deftest get-json-weather-report-feature-settings-test
    (testing "Evaluate the result of the two reports when providing one feature and multiple settings"
        (let [response1 (get-json-weather-report-type1 ["conditions"] ["lang:FR" "pwd:1"] "CA/San_Francisco")]
            (let [response2 (get-json-weather-report-type2 "conditions" "lang:FR" "pwd:1" "q/CA/San_Francisco")]
                    (is (.equals (:body response1) (:body response2)))))))

(deftest get-json-weather-report-features-setting-test
    (testing "Evaluate the result of the two reports when providing multiple features and one setting"
        (let [response1 (get-json-weather-report-type1 ["conditions" "forecast"] ["lang:FR"] "CA/San_Francisco")]
            (let [response2 (get-json-weather-report-type2 "conditions" "forecast" "lang:FR" "q/CA/San_Francisco")]
                    (is (.equals (:body response1) (:body response2)))))))

(deftest get-json-weather-report-features-settings-test
    (testing "Evaluate the result of the two reports when providing multiple features and multiple settings"
        (let [response1 (get-json-weather-report-type1 ["conditions" "forecast"] ["lang:FR" "pwd:1"] "CA/San_Francisco")]
            (let [response2 (get-json-weather-report-type2 "conditions" "forecast" "lang:FR" "pwd:1" "q/CA/San_Francisco")]
                    (is (.equals (:body response1) (:body response2)))))))

(deftest get-lang-test
    (testing "Testing the language extractions function"
        (is (= "FR" (get-lang "conditions/lang:FR/?location=NY")))
        (is (= "FR" (get-lang "conditions/lang:FR")))
        (is (= "FR" (get-lang "conditions/lang:FR/pwd:0/")))
        (is (= nil (get-lang "conditions/")))
        (is (= "URL string is empty" (get-lang "")))
        ))