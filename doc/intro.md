# Introduction to weather-app
It is very simple functionalities of the weather app written in Clojure.
It retrieves the URL from the weather API. It also has a function to extract the language from a given string in the weather API format (Features/Settings/location)

### Run App
+ In a terminal run `lein repl`

### Run Tests
+ In a terminal run `lein test`
**NOTE** All the tests should pass. In case of failure, it could be due to the rate-limitation on the API key.
