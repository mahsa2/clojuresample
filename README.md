# weather-app

An app which makes an API calls to the [www.wunderground.com API](https://www.wunderground.com/weather/api/d/docs?MR=1)

## Documentations

Please look at the `doc/intro.md` file

## License

Copyright © 2016 Mahsa Mohammadkhani
